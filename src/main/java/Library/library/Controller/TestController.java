package Library.library.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RequestMapping("api")

@RestController
public class TestController {
    @GetMapping("/greet")
    public String greet() {
        return "Hello, World!";
    }
}
