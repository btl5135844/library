package Library.library.UserManager.UserRepository;

import Library.library.UserManager.Entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
public interface UserRepo  extends CrudRepository<User,Integer>  {
    Optional<User> findByEmail(String email);
}
