package Library.library.UserManager.UserRepository;

import Library.library.UserManager.Entity.Role;
import Library.library.UserManager.Entity.RoleEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepo extends CrudRepository<Role, UUID> {
    Optional<Role> findByName(RoleEnum name);
}
