package Library.library.UserManager.Entity;

public enum RoleEnum {
    USER,
    ADMIN,
    SUPER_ADMIN
}
