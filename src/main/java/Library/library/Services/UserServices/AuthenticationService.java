package Library.library.Services.UserServices;

import Library.library.DTO.Userdto.LoginDtos;
import Library.library.DTO.Userdto.RegisterUserDto;
import Library.library.UserManager.Entity.Role;
import Library.library.UserManager.Entity.RoleEnum;
import Library.library.UserManager.Entity.User;
import Library.library.UserManager.UserRepository.RoleRepo;
import Library.library.UserManager.UserRepository.UserRepo;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {
    private final UserRepo userRepository;
    private  final RoleRepo roleRepository;
    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    public  AuthenticationService(UserRepo userRepo,PasswordEncoder passwordEncoder,AuthenticationManager authenticationManager
    ,RoleRepo roleRepository){
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository=roleRepository;
    }
    public User signup(RegisterUserDto input) {
        Optional<Role> optionalRole = roleRepository.findByName(RoleEnum.USER);

        if (optionalRole.isEmpty()) {
            return null;
        }
        var user = new User();
                user.setFullName(input.getFullName());
                user.setEmail(input.getEmail());
                user.setPassword(passwordEncoder.encode(input.getPassword()));
                user.setRole(optionalRole.get());


        return userRepository.save(user);
    }

    public User authenticate(LoginDtos input) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        input.getEmail(),
                        input.getPassword()
                )
        );

        return userRepository.findByEmail(input.getEmail())
                .orElseThrow();
    }
}
